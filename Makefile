current: 
	echo make pd_darwin, pd_linux, pd_nt, pd_irix5, or pd_irix6

#
# To compile:  make clean make pd_darwin
# ----------------------- Mac OSX -----------------------

pd_darwin: roughometer.pd_darwin

.SUFFIXES: .pd_darwin
# -fast -ffast-math 
DARWININCLUDE =  -I/Applications/Pd-0.51.4.app/Contents/Resources/src/
DARWINCFLAGS = -O2 -DPD -Wall -W -Wshadow -Wstrict-prototypes \
    -Wno-unused -Wno-parentheses -Wno-switch -arch i386 -arch x86_64

.c.pd_darwin:
	#mv *.pd_darwin *.pd_darwin~
	cc $(DARWINCFLAGS) $(DARWININCLUDE) -o $*.o -c $*.c
	cc -bundle -undefined suppress -flat_namespace -arch i386 -arch x86_64 -o $*.pd_darwin $*.o 
	rm -f $*.o

#-------Documentation-----
doc:
	/Applications/Doxygen.app/Contents/Resources/doxygen Doxyfile.cnf
	/usr/local/bin/astyle --style=java --suffix=none --errors-to-stdout *.c 
# ----------------------- NT -----------------------

pd_nt: $(NAME).dll

.SUFFIXES: .dll

PDNTCFLAGS = /W3 /WX /O2 /DNT /DPD /nologo

# where is VC++ ???
VC="C:\Program Files\Microsoft Visual Studio 8\VC"

# where is your m_pd.h ???
PDNTINCLUDE = /I. /I"C:\Program Files\pd\tcl\include" /I"C:\Program Files\pd\src" /I$(VC)\include /Iinclude
PDNTLDIR = $(VC)\lib
#PDNTLIB = $(PDNTLDIR)\libc.lib \        
#            $(PDNTLDIR)\user32.lib \
#            $(PDNTLDIR)\uuid.lib \
PDNTLIB =    $(PDNTLDIR)\kernel32.lib \
             $(PDNTLDIR)\oldnames.lib \
             $(PDNTLDIR)\LIBCMT.lib \
             "C:\Program Files\pd\bin\pd.lib" \

.c.dll:
	cl $(PDNTCFLAGS) $(PDNTINCLUDE) /c $*.c
	link /dll /export:$(CSYM)_setup $*.obj $(PDNTLIB)
# ----------------------------------------------------------

install:
	cp help-*.pd ../../doc/5.reference

clean:
	rm -f *.o *.pd_* so_locations
