/** \mainpage Calculating auditory roughness by spectral approach.
	\section Description
	This class receives up to 20 streams of amplitudes and frequencies obtained
	from sigmund~, or observing its same structure, and whenever a bang message
	is sent to the leftmost inlet it returns the computed roughness value.
	The creation arguments are the number of audio streams.

	\section Licence
	\Copyright (C) 2008  Julian Villegas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
	any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

	@author Julian Villegas <julovi ^_^ at ^_^ yahoo (*) com>
	@version 0.2a
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "m_pd.h"
#define MAX_INSTRUMENTS 20  ///< Max. number of streams
#define MAX_PARTIALS 20///< Max. number of spectra components
#define SCALE_CONSTANT 100///< A scale constant to express the roughness in manageable values.
#define DEFAULT_THRESHOLD 0.001 ///< Default amplitude threshold for incoming streams

//#define WIN // Uncomment to compile in Windows

#ifndef WIN
#define min(a,b) \
       ({ typeof (a) _a = (a); \
           typeof (b) _b = (b); \
         _a < _b ? _a : _b; }) ///< Returns the minimum of the two arguments

#define max(a,b) \
       ({ typeof (a) _a = (a); \
           typeof (b) _b = (b); \
         _a > _b ? _a : _b; }) ///< Returns the maximum of the two arguments
#endif

static t_class *roughometer_class;
static t_class *proxy_class;

typedef struct _roughometer {
    t_object x_obj;
    t_float T[MAX_INSTRUMENTS][MAX_PARTIALS]; ///< Frequencies matrix
    t_float A[MAX_INSTRUMENTS][MAX_PARTIALS]; ///< Amplitudes matrix
    t_float totalRoughness;///< Resulting roughness
    t_int compFlag; ///< Flag for calculation
    t_int componentCounter[MAX_INSTRUMENTS]; ///< counters for calculation
    t_int iterations;
    t_int numInstruments; ///< User defined number of instruments
    t_int numPartials;///< User defined number of spectral components
    t_int initialConditions;
    t_float threshold;///< Amplitude threshold defined by the user
    t_outlet *f_out1;///< outlet to output the list of new tunings
} t_roughometer;

typedef struct proxy {
    t_object obj;
    t_int index;  ///< index of the proxy inlet
    t_roughometer *x;	///< A pointer to the main data structure
} t_proxy; ///< Auxiliary structure to allow dynamic number of inlets

/** Calculate S
 *
 * \f[
 * 	S=\frac{0.24}{0.0207 f + 18.96}
 *  \f]
 * @param f the minimum frequency of a dyad.
 * @return S
 * @see dissonance()
 */
t_float S(t_float f) {
    return 0.24/(0.0207*f+18.96);
}
/** Calculates auditory roughness of a dyad using the model proposed by Vassilakis.
 *
 * Vassilakis models assumes that for a dyad \f$(f_1,a_1)\f$ and \f$f_2,a_2)\f$, \f$(a_1 \geq a_2)\f$.
 * There's an error in his thesis about this (p. 197). The SRA model is better.
 *
 * See also Sethares' "Adaptive tunings for musical scales."
 *
 * This method calculates \f[
 *	d=\frac{1}{2} {(a_1 a_2)^{0.1}} {\left(
 *								\frac{2\,min(a_1 , a_2)}{a_1 + a_2}
 *								\right) ^{3.11}} (e^{-3.5 F} - e^{-7.5 F})
 * \f] where
 * \f[
 *	F= S(min(a_1,a_2)) \left| f_1 - f_2 \right|
 * \f]
 * Components with frequencies beyond the default boundaries (amp <=threshold, 20Hz  < f < 20kHz)
 * don't contribute in the result.
 * @param *x A pointer to the data structure of the class
 * @param a1 The amplitude of the first spectral component
 * @param a2 The amplitude of the second spectral component
 * @param f1 The frequency of the first spectral component
 * @param f2 The frequency of the second spectral component
 * @return the sensory roughness of the dyad
 * @see d_Calculation(), S()
 */
t_float dissonance(t_roughometer *x,t_float a1,t_float a2,t_float f1,t_float f2) {
    t_float F=0;
    // Vassilakis models assumes that for a dyad {f1,a1} and {f2,a2}, a1>=a2.
    //There's an error in his thesis about this (p. 197). The SRA model is better.
    //See also Sethares' "Adaptive tunings for musical scales"
    if ((a1<=x->threshold) || (a2<=x->threshold) || (f1==0) || (f2==0))
        return 0;
    else {
        F =  S(min(f1,f2)) * fabs(f1-f2);
        return	SCALE_CONSTANT * 0.5 * pow(a1*a2,0.1) *
                pow(2*min(a1,a2)/(a1+a2),3.11) *
                (exp(-3.5 * F) - exp(-5.75 * F));
    }
}
/** Calculates the complex roughness of the ensemble.
 * One difference between the model proposed by Vassilakis is that only the dissonance
 * produced by components belonging to different signals count. In other words, the
 * intrinsic dissonance is not considered because it is wanted to preserve the timbre
 * of the signal. Also, by doing it the number of iterations is reduced. This function
 * is the kernel of the minimization, since it takes the current values of the vicinity
 * in cents for the stream where an onset was detected and calculates
 *
 *  \f[
 *	d= \sum_{h=0}^{n-1} \sum_{i=h}^{n} \sum_{j=0}^{p} \sum_{k=0}^{p}
 *  dissonance( a_1,a_2,F_1,F_2)
 * \f]
 * where  \f$n,p,F_1,F_2\f$ are the number of streams, number of partials,
  \f$f_{(1,2)}\, \times vicinity\f$ if \f$f_{(1,2)} \in \f$ onsetting stream, or \f$f_{(1,2)}\f$ otherwise
 * @param x A pointer to the inner structure of the object.
 * @return the roughness for a complex tone ensamble
 */
t_float d_Calculation(t_roughometer *x) {
    t_int h,i,j,k;
    t_float d=0.0;
    for (h=0; h < x->numInstruments; h++)
        for (i=0; i < x->numInstruments; i++) //I think it should be i=h, but this is how Sethares has it.
            for (j=0; j < x->numPartials; j++)
                for (k=0; k < x->numPartials; k++)
                    d+=dissonance(x,x->A[h][j],x->A[i][k],
                                  x->T[h][j],x->T[i][k]);
    d /= 10.7; /// To make R of an 1kHz AM tone @ 70 Hz = 1
    return d;
}
/** Initializes the frequency and amplitude matrices as well as the solution, partial solution, and last pitch vector.
 * @param x A pointer to the inner structure of the object
 */
void roughometer_initMatrix(t_roughometer *x) {
    t_int i=0;
    t_int j=0;
    for (i=0; i<x->numInstruments; i++)
        for (j=0; j<MAX_PARTIALS; j++)
            x->A[i][j]=x->T[i][j]=0;
}

/** Sets the counters of processed income to the value indicated by the parameter 'value.'
 * @param x a pointer to the inner structure of the object
 * @param value the new value for the counters
 */
void setCounters(t_roughometer *x,t_int value) {
    t_int i;
    for (i=0; i< x->numInstruments; i++)
        x->componentCounter[i]=value;
}

/** Process a request for roughness calculation.
* @param x A pointer to the inner structure of the object
 */
void roughometer_bang(t_roughometer *x) {
    x->compFlag=1; ///< set the flag to read the components
    setCounters(x,0); ///< reset the counters
}
/** Sets the minimum amplitude value to be considered for the Roughness calculation.
 * When working with soundfiles or captured audio, there is a noise that can
 * distort the roughness calculation. This can be compensated by adjusting
 * the threshold.
 * @param x a pointer to the inner structure of the object
 * @param f the new threshold in part per thousand
 */
void roughometer_setOffset(t_roughometer *x,t_floatarg f) {
    x->threshold= f/1000;
}

/** Counts how many streams have been completed (in terms of the number of partials analyzed).
 * and returns 0 or 1 if there are some incomplete or not respectevly.
 * @param x a pointer to the inner structure of the object
 */
t_int areAllComponentsIn(t_roughometer *x) {
    t_int i,j=0;
    for (i=0; i< x->numInstruments; i++) {
        if (x->componentCounter[i]<x->numPartials) {
            j++;
        }
    }
    if (j>0)	{
        return 0; ///> not all the components have been written
    } else {
        return 1; ///> they're all in.
    }
}

/** Update the frequency and amplitude values for each instrument.
 * The list of arguments consists of the following elements:
 * inlet_id, partial_id, frequency, amplitude, cos, sin or
 * inlet_id, partial_id, frequency, amplitude, flag depending on
 * the argument of sigmund~ (peaks or tracks, peaks are preferred).
 * inlet_id corresponds to the number of the instrument.
 *
 * @param p the inlet that is calling the method
 * @param argc the number of elements in the list of arguments
 * @param argv the list of arguments
 */
static void roughometer_value(t_proxy *p, t_symbol *s, int argc, t_atom *argv) {
    t_int i,j=0;
    t_roughometer *x = (t_roughometer *)(p->x);
    if (x->compFlag==1) // update the matrices
        for (i=0; i<argc; i++) {
            switch (i) {
            case 0: ///> the frequency component
                j=(t_int)atom_getfloatarg(i,argc,argv);
                x->componentCounter[p->index]++;
                break;
            case 1: ///> the frequency value
                x->T[p->index][j]= atom_getfloatarg(i,argc,argv);
                break;
            case 2: ///> the amplitude value
                x->A[p->index][j]=atom_getfloatarg(i,argc,argv);
                break;
            default:
                break;
            }
        }
    if (x->compFlag==1 && areAllComponentsIn(x)) {
        x->compFlag=0; ///clear the flag
        outlet_float(x->f_out1,d_Calculation(x)); /// do the minimization
    }
}
/** Instantiate the object
 * @param n number of spectra
 * @param c vicinity in cents
 */
void *roughometer_new(t_floatarg n) {
    int i;
    t_roughometer *x = (t_roughometer *)pd_new(roughometer_class);
    t_proxy *inlet[MAX_INSTRUMENTS];
    for (i=0; i<MAX_INSTRUMENTS; i++)
        inlet[i] = (t_proxy *)pd_new(proxy_class);
    if ((n>MAX_INSTRUMENTS) || (n<0) ) {
        x->numInstruments = MAX_INSTRUMENTS;
        post("The maximum number of spectra is %d",MAX_INSTRUMENTS);
    } else {
        x->numInstruments = n;
    }
    if (!n)
        x->numInstruments=1;
    x->numPartials=MAX_PARTIALS;
    x->initialConditions=1;
    x->threshold=DEFAULT_THRESHOLD;
    roughometer_initMatrix(x);
    for (i=0; i<x->numInstruments; i++) {
        inlet[i]->x = x;
        inlet[i]->index = i;
        inlet_new(&x->x_obj, &inlet[i]->obj.ob_pd, 0,0);
    }

    x->f_out1 = outlet_new(&x->x_obj, &s_float);
    return (void *)x;
}
/**
 * setup the object
 */
void roughometer_setup(void) {
    t_symbol *symbol;
    roughometer_class = class_new(gensym("roughometer"),
                                  (t_newmethod)roughometer_new,
                                  0,sizeof(t_roughometer),
                                  CLASS_DEFAULT,A_DEFFLOAT,0);
    proxy_class = class_new(gensym("roughometer_proxy"), NULL, NULL, sizeof(t_proxy),
                            CLASS_PD|CLASS_NOINLET, A_NULL);

    class_addbang(roughometer_class,(t_method)roughometer_bang);
    class_addfloat(roughometer_class,(t_method)roughometer_setOffset);
    class_addanything(proxy_class, (t_method)roughometer_value);

    symbol = gensym("roughometer.pd");
    class_sethelpsymbol(roughometer_class, symbol);
}
